( pcb2gcode 1.3.2 )
( Software-independent Gcode )

G94 ( Millimeters per minute feed rate. )
G21 ( Units == Millimeters. )

G90 ( Absolute coordinates. )
S10000 ( RPM spindle speed. )
G64 P1.00000 ( set maximum deviation from commanded toolpath )
F600.00000 ( Feedrate. )

F600.00000 ( Feedrate. )
M3 ( Spindle on clockwise. )
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X4.20523 Y27.82626 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X4.20523 Y27.82626
X1.32625 Y29.74643
X3.24641 Y32.62541
X6.12540 Y30.70525
X4.20523 Y27.82626
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X14.52114 Y27.88041 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X14.52114 Y27.88041
X11.16240 Y30.12052
X13.40250 Y33.47926
X16.76124 Y31.23916
X14.52114 Y27.88041
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X25.58711 Y27.94242 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X25.58711 Y27.94242
X22.19240 Y28.61455
X22.86453 Y32.00924
X26.25922 Y31.33713
X25.58711 Y27.94242
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X20.91762 Y15.68209 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X20.91762 Y15.68209
X24.99806 Y17.37657
X28.43772 Y15.19588
X28.43079 Y24.76272
X13.43895 Y24.75581
X13.26989 Y12.92281
X14.61337 Y12.87069
X15.87189 Y15.98232
X20.91762 Y15.68209
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X17.30899 Y9.62772 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X17.30899 Y9.62772
X19.30292 Y10.44598
X16.92619 Y10.82772
X16.29853 Y15.24036
X15.90178 Y11.50034
X13.97585 Y9.57433
X12.47774 Y10.14532
X12.92076 Y3.89319
X15.05845 Y3.92077
X14.57580 Y8.47730
X17.30899 Y9.62772
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X13.96183 Y11.72401 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X13.96183 Y11.72401
X12.31121 Y13.39258
X12.57796 Y25.62403
X28.37798 Y25.65235
X28.39152 Y34.21761
X-0.01597 Y34.14154
X-0.04152 Y15.63886
X4.49805 Y17.37657
X7.62402 Y14.87564
X7.72270 Y3.66690
X12.04126 Y3.72710
X11.72018 Y10.45792
X13.96183 Y11.72401
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X14.70298 Y2.82580 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X14.70298 Y2.82580
X7.10152 Y3.08397
X5.79197 Y15.78615
X2.76787 Y16.42410
X0.00486 Y13.99421
X0.08205 Y-0.02768
X28.27379 Y-0.04920
X28.43770 Y14.25580
X24.53936 Y16.55935
X18.22616 Y14.26594
X17.91737 Y12.27181
X20.12570 Y9.85170
X15.66673 Y8.43184
X14.70298 Y2.82580
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z5.00000 ( retract )

G00 X22.07533 Y3.45100 ( rapid move to begin. )
F300.00000
G01 Z-0.50000
G04 P0 ( dwell for no time -- G64 should not smooth over this point )
F600.00000
G01 X22.07533 Y3.45100
X24.95099 Y5.37632
X26.87631 Y2.50068
X24.00065 Y0.57536
X22.07533 Y3.45100

G04 P0 ( dwell for no time -- G64 should not smooth over this point )
G00 Z10.000 ( retract )

M5 ( Spindle off. )
M9 ( Coolant off. )
M2 ( Program end. )

